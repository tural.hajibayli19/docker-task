=============================================
Create container with sonatype/nexus3
This container must map his 8081 port to 80 port of host (wm)
    Steps:
--- $docker run -d -p 80:8081 -p 8082:8082 --name nexus-server sonatype/nexus3
=============================================
Then in browser connect to sonatype ui
For admin password you must connect to container and get password
    Steps:
--- $docker exec -it containerID cat /nexus-server/admin.password
=============================================
After successful login create new repo with docker hosted type
Create sample nginx image which will show you "Hi from container" sample welcome page
And push it to sonatype.
    Steps:
--- $docker run -d -p 81:80 --name nginx-server nginx
--- $echo "Hi from container" > index.html
--- $docker cp index.html containerID:/usr/share/nginx/html/index.html
--- $docker login -u admin *ipaddress*:8082
--- $docker tag nginx:latest *ipaddress*:8082/nginx
--- $docker push *ipaddress*:8082/nginx
    Result:
[neo@fedora-srv ~]$ docker push 172.20.10.149:8082/nginx
Using default tag: latest
The push refers to repository [172.20.10.149:8082/nginx]
3c9d04c9ebd5: Pushed 
434c6a715c30: Pushed 
9fdfd12bc85b: Pushed 
f36897eea34d: Pushed 
1998c5cd2230: Pushed 
b821d93f6666: Pushed 
24839d45ca45: Pushed 
latest: digest: sha256:1bb5c4b86cb7c1e9f0209611dc2135d8a2c1c3a6436163970c99193787d067ea size: 1778

